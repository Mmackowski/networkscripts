ip netns add N1
ip link add tap1 type veth peer name btap1
ip link set tap1 netns N1

echo "ovs"
ovs-vsctl add-br br0 \
		-- set bridge br0 datapath_type=netdev\
		-- add-br br1 \
		-- set bridge br1 datapath_type=netdev\
		-- add-port br1 dpdk0 \
		-- set Interface dpdk0 type=dpdk\
		-- add-port br0 btap1 tag=100 \
		-- add-port br0 vx1\
		-- set Interface vx1 type=vxlan options:remote_ip=60.0.0.1

echo "ip"
ip link set br0 up
ip link set br1 up
ip link set btap1 up
ip netns exec N1 ip link set tap1 up

ip a a 60.0.0.2/24 dev br1
ip netns exec N1 ip a a 10.0.0.2/24 dev tap1
