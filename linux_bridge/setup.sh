#!/bin/bash

#set Namespaces

ip netns add N1
ip netns add N2

#set bridge
brctl addbr br0
ip link set dev br0 up

#set one veth pair
ip link add tap1 type veth peer name br-tap1
brctl addif br0 br-tap1
ip link set tap1 netns N1
ip netns exec N1 ip link set dev tap1 up
ip netns exec N1 ip addr add 10.0.0.1/24 dev tap1
ip link set dev br-tap1 up


#set secound veth pair
ip link add tap2 type veth peer name br-tap2
brctl addif br0 br-tap2
ip link set tap2 netns N2
ip netns exec N2 ip link set dev tap2 up
ip netns exec N2 ip addr add 10.0.0.2/24 dev tap2
ip link set dev br-tap2 up

