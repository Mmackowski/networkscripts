#!/bin/bash

#set Namespaces

ip netns add N1
ip netns add N2
ip netns add N3
ip netns add N4


#set bridge
brctl addbr br0
ip link set dev br0 up

ip link add tap1 type veth peer name br-tap1
ip link add tap2 type veth peer name br-tap2
ip link add tap3 type veth peer name br-tap3
ip link add tap4 type veth peer name br-tap4

brctl addif br0 br-tap1
brctl addif br0 br-tap2
brctl addif br0 br-tap3
brctl addif br0 br-tap4

ip link set dev tap1 netns N1
ip link set dev tap2 netns N2
ip link set dev tap3 netns N3
ip link set dev tap4 netns N4

ip netns exec N1 vconfig add tap1 100
ip netns exec N2 vconfig add tap2 100
ip netns exec N3 vconfig add tap3 200
ip netns exec N4 vconfig add tap4 200

ip link set dev br-tap1 up
ip link set dev br-tap2 up
ip link set dev br-tap3 up
ip link set dev br-tap4 up

ip netns exec N1 ip link set dev tap1 up
ip netns exec N2 ip link set dev tap2 up
ip netns exec N3 ip link set dev tap3 up
ip netns exec N4 ip link set dev tap4 up

ip netns exec N1 ip link set dev tap1.100 up
ip netns exec N2 ip link set dev tap2.100 up
ip netns exec N3 ip link set dev tap3.200 up
ip netns exec N4 ip link set dev tap4.200 up

ip netns exec N1 ip addr add 10.0.0.1/24 dev tap1.100
ip netns exec N2 ip addr add 10.0.0.2/24 dev tap2.100
ip netns exec N3 ip addr add 10.0.0.1/24 dev tap3.200
ip netns exec N4 ip addr add 10.0.0.2/24 dev tap4.200





