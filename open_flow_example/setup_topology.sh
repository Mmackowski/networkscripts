#!/bin/bash

# creating namespaces
ip netns add N1
ip netns add N2
ip netns add N3
ip netns add N4
ip netns add N5

#creating veth pairs
ip link add tap1 type veth peer name btap1
ip link add tap2 type veth peer name btap2
ip link add tap3 type veth peer name btap3
ip link add tap4 type veth peer name btap4
ip link add tap5 type veth peer name btap5

#setting linux netspaces for created interfaces
ip link set dev tap1 netns N1
ip link set dev tap2 netns N2
ip link set dev tap3 netns N3
ip link set dev tap4 netns N4
ip link set dev tap5 netns N5

#creating virtual switch and adding ports to it
ovs-vsctl add-br br3 \
	-- add-port br3 btap1 \
	-- add-port br3 btap2 \
	-- add-port br3 btap3 tag=3 \
	-- add-port br3 btap4 tag=4 \
	-- add-port br3 btap5 tag=5 

#adding open flow rules for br3
#notice opent flow port number might vary
ovs-ofctl add-flow br3 in_port=3,actions=strip_vlan,output:4,output:5
ovs-ofctl add-flow br3 in_port=4,actions=strip_vlan,output:3
ovs-ofctl add-flow br3 in_port=5,actions=strip_vlan,output:3

#adding ip's to interfaces in namespaces
ip netns exec N1 ip a a 10.0.0.1/24 dev tap1
ip netns exec N2 ip a a 10.0.0.2/24 dev tap2
ip netns exec N3 ip a a 10.0.0.3/24 dev tap3
ip netns exec N4 ip a a 10.0.0.4/24 dev tap4
ip netns exec N5 ip a a 10.0.0.5/24 dev tap5

#setting up all network interfaces
ip netns exec N1 ip link set dev tap1 up
ip netns exec N2 ip link set dev tap2 up
ip netns exec N3 ip link set dev tap3 up
ip netns exec N4 ip link set dev tap4 up
ip netns exec N5 ip link set dev tap5 up
ip link set dev btap1 up
ip link set dev btap2 up
ip link set dev btap3 up
ip link set dev btap4 up
ip link set dev btap5 up
ip link set dev br3 up
