
ip netns add N1
ip netns add N2

ovs-vsctl add-br br0

ovs-vsctl add-port br0 tap1 -- set Interface tap1 type=internal
ovs-vsctl add-port br0 tap2 -- set Interface tap2 type=internal

ip link set dev tap1 netns N1
ip link set dev tap2 netns N2

ip netns exec N1 ip link set dev tap1 up
ip netns exec N2 ip link set dev tap2 up

ip netns exec N1 ip addr add 10.0.0.1/24 dev tap1
ip netns exec N2 ip addr add 10.0.0.2/24 dev tap2
